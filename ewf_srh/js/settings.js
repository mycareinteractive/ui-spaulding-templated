var Settings = View.extend({
    
    id: 'settings',
        
    template: 'settings.html',
    
    css: 'settings.css',
    
    webpage: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
	navigate: function(key) {
        var curr = $(".settings #options a.active");

        if (curr.length>=0 && (key=='ENTER')) {  
            return this.click(curr,key);
        }
    
        if ( key=='MENU' || key =='HOME') {
            this.destroy();
            return false;
        }

        if ( key=='POWR' ||	key=='END')	{
            this.destroy();
            return false;
        }
    
        var navkeys = ["UP", "DOWN", "LEFT", "RIGHT"];
    
        if(navkeys.indexOf(key) != -1) {
            this.changeDataFocus(key); 
            return true;
        }
    
        return false;
        
    },
	    
    click: function($jqobj,key) {
        var navigation  = ['noimages','lightdark','darklight','hearaudio','save','cancel'];
        var activeClass = navigation[$jqobj.index('.settings #options a')];
		
        if(activeClass == 'cancel' || key=='LEFT') { // back button
            this.destroy();
            return true;
        } 
        else if(activeClass == 'save') {
        
            var noimages = false, lightdark = false, darklight = false, audio = false;
        
            if ($(".settings a.noimages").hasClass('selected'))
                noimages      = true;
            
            if ($(".settings a.lightdark").hasClass('selected'))
                lightdark    = true;          
            else if ($(".settings a.darklight").hasClass('selected'))
                darklight    = true;
                      
            if ($(".settings a.hearaudio").hasClass('selected'))
                audio       = true;

            cacheSettings(noimages, lightdark, darklight, audio);
            submitConfigs(noimages, lightdark, darklight, audio);
            this.destroy();
            return true;
        } 
    
        if ($jqobj.hasClass('selected')) {
            $jqobj.removeClass('selected');
            $(".settings").removeClass(activeClass);
        }   
        else {
            $jqobj.addClass('selected');
            $(".settings").addClass(activeClass);
        
            // lightdark and darklight are exclusive, you can't have both
            if (activeClass=='lightdark' && $(".settings a.darklight").hasClass('selected')) {
                $(".settings a.darklight").removeClass('selected');
                $(".settings").removeClass('darklight');
            }   
            else if (activeClass=='darklight' && $(".settings a.lightdark").hasClass('selected')) {
                $(".settings a.lightdark").removeClass('selected');
                $(".settings").removeClass('lightdark');
            }
        }
    
    return true;
            
    },

    
    renderData: function() {
        $('#' + this.wrapper + " .settings").addClass(this.classStr);
        var data = this.data;

        var firstobj = $('div.settings div#options a:first-child');            
        this.focus(firstobj);
        var context = this;
        // wire up the mouse event
        $('div.settings div#options a').click(function(){
            context.blur();
            context.focus($(this));
            context.click($(this));
            return false;
        });
            
        // now we take over the key handler from navigation.js
        //this._addKeyListener();
				      
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    shown: function() {
        var $firstObj =  $('div.settings div#options a:first-child');            
        this.focus($firstObj);
        //$firstObj.click();		
		    // show the div
        $('#' + this.wrapper + " .settings").show();
        var settings = getSettings();
		
        if (settings.length>0) {
            $(".settings").addClass(settings);
        
            if(settings.indexOf('hearaudio') != -1)
                $('div.settings div#options a.hearaudio').addClass('selected');
            
            if(settings.indexOf('lightdark') != -1)
                $('div.settings div#options a.lightdark').addClass('selected');
            else if(settings.indexOf('darklight') != -1)
                $('div.settings div#options a.darklight').addClass('selected');
            
            if(settings.indexOf('noimages') != -1) {
                $('div.settings div#options a.noimages').addClass('selected');
			}
        }
    

    },
     
    focus : function(jqobj) {
        jqobj.addClass('active');
    },

    // When an object lose focus or mouse leave
    blur : function(jqobj) {
        if(jqobj) {
            jqobj.removeClass('active');
        }
        else {
            $('div.settings div#options a.active').removeClass('active');
        }
        this.idx = -1;
    },

    changeDataFocus  : function(key) {       
    
        var curr = $('div.settings div#options a.active');
        var next = curr;

        if(curr.length <= 0) {
            this.idx = -1;
            return;
        }
        
        switch(key) {
            case 'UP':
            case 'LEFT':
                next = curr.prevAll('a').first();
                if(next.length<=0)
                    next = $('div.settings div#options a:last-child');
                break;
            case 'DOWN':
            case 'RIGHT':
                next = curr.nextAll('a').first();
                if(next.length<=0)
                    next = $('div.settings div#options a:first-child');
                break;
        }
    
        this.blur(curr);
        this.focus(next);
    }
});



