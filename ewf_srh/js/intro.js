var Intro = View.extend({
    
    id: 'intro',
    
    template: 'intro.html',
    
    css: 'intro.css',
    
    className: 'languages',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
		var $curr = $('#intro a.active');
		var $next = $();
		
		if($curr.is(':first-child') && (key == 'LEFT' || key == 'UP'))  
			$next = $curr.siblings(':last');
		else if ($curr.is(':last-child') && (key == 'RIGHT' || key == 'DOWN')) 
			$next = $curr.siblings(':first');
		else if (key == 'LEFT' || key == 'UP')
			$next = $curr.prev();
		else 
			$next = $curr.next();
		
		if($next.length<=0) 
			$next = $curr.prev();
        if(key == 'POWR') {
            return false;
        }
        var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }
		else if (key == 'ENTER') {  // default link click             								
			return this.click($curr);
		}
						
		else if(key == 'UP' || key == 'DOWN' || key == 'RIGHT' || key == 'LEFT') {			
            if($next.length > 0) {
                this.blur($curr);
                this.focus($next);
            }
		}                
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        window.settings = window.settings || {};
        window.settings.language = 'en';        

        if (linkid == 'mycare') {
            // tracking
            nova.tracker.event('intro', 'mycare');

            // close both intro page and intro2 page
            this.destroy();

            // build primary page
            var page = new Primary({});
            page.render();
            return true;
        }
        else if (linkid == 'watchtv') {
            nova.tracker.event('intro', 'watchtv');
            // Open TV guide
            this.watchTV(linkid);
            return true;
        }

		else if (linkid == 'settings') {
            nova.tracker.event('intro', 'settings');
            // Open Settings
            this.openSettings(linkid);
            return true;
        }

        return false;
    },
    
	shown: function () {
		applySettings('#intro');
    },
	
    renderData: function() {
        var context = this;
        var ewf = ewfObject();     
        /*
		msg(ewf.remote);
        
		this.$('#remote').addClass(ewf.remote);
        
        if(ewf.remote!='') {            
            var div = '#instruction.'+ewf.remote;                 
            this.$('#instruction').hide();
            this.$(div).show();            
        } else {
            this.$('#instruction.swingarm').hide();            
        }*/        
        
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    watchTV: function (className) {
        //var context = this;
        //
        //this.tvChannels = [];
        // var channels = epgChannels().Channels;
        // $.each(channels, function(i, ch) {
        //     context.tvChannels.push({type: 'Analog', url: ch.channelNumber, display: ch.channelName});
        // });
        // var page = new VideoPlayer2({viewId: 'tvonly', className:className, playlist: this.tvChannels, playlistIndex: 0, delay: 10000, delayMessage: 'One moment while we are loading the television channels...'});
        // page.render();
        setLanguage(''); 
        var linkId = 'tv';
        var pagePath = this.pagePath + '/' + linkId;
        var page = new TVGuide({className: className, pagePath: pagePath});
        page.render();
    },
	
	openSettings: function (linkid, breadcrumb, data, pagePath) {        
        var context = this;
        var page = new Settings({
            className: linkid, breadcrumb: breadcrumb, data: data, pagePath: pagePath,
            oncreate: function () {                
            },
            ondestroy: function () {
				applySettings('#intro');
            }
        });
        page.render();
    }
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});

