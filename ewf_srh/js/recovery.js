var Recovery = View.extend({

    id: 'recovery',
    
    template: 'recovery.html',
    
    css: 'recovery.css',
    
    pageSize: 6,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
			var $curr = $("#recovery .major a.selected");			
			this.key = key;						
			var context = this;
			
			if(key == 'ENTER' && $curr.hasClass('back-button')) {
				this.destroy();
				return true;
			}				
			else if(key == 'MENU' || key == 'HOME') {
				this.destroy();
				return true; 			
			}
			else if(key == 'UP' || key == 'DOWN') {
				this.changeFocus(key, '.major','','.selected');   
				return true;
			}
            else if (key == 'CHUP' || key == 'PGUP') {
                this.focusPrevPage('.major', 9, null, 'vertical', '', '.selected');
                return true;
            }
            else if (key == 'CHDN' || key == 'PGDN') {
                this.focusNextPage('.major', 9, null, 'vertical', '', '.selected');
                return true;
            }
			else if(key == 'LEFT') {
				this.destroy();
				return true;
			}	
			else if ((key == 'ENTER' || key == 'RIGHT' ) && !$curr.hasClass('back-button')) {  // default link click             					
					this.key = 'ENTER';
					return this.click($curr);
			}
			return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        var itemData = this._getMenuItemData($jqobj);
        	
    	if($jqobj.hasClass('back-button') && this.key == 'ENTER') { // back button
    		this.destroy();
    		return true;
    	} 
    	
    	
    	// selecting a video
    	if($jqobj.hasClass('asset') && itemData && this.key == 'ENTER') {
    	    var breadcrumb = this.$('.page-title').text();
    	    var page = new RecoveryDetail({className:'', parent:this, breadcrumb:breadcrumb, data: itemData, label: this.data.label});
    	    page.render();
    	    return true;
    	}
    	
    	return false;
    },
    
    renderData: function() {
		this.label = this.data.label;
        //this.$('.page-title').html(this.breadcrumb);        
		this.$('.page-title').html(this.label);                
		var c = $('<p class="text-large"></p>').html(this.label);
        //this.$('#heading2').append(c);
        //this.label = this.data.label;
		var ewf = ewfObject();		
		this._buildAssets(ewf.recovery);        
    },
    
    shown: function() {
		var $firstObj = this.$('.major a:nth-child(1)');		
        if($firstObj.length == 0) 
            $firstObj = this.$('.major .back-button');		
        
        $firstObj.addClass('selected');
		applySettings('#recovery');
		this.focus($firstObj);
    },
    
    refresh: function() {
        var context = this;
        context.$('.major').empty();
        var $firstObj = this.$('.major a:nth-child(2)');		
        if($firstObj.length == 0) 
            $firstObj = this.$('.major .back-button');		
        
        $firstObj.addClass('selected');
        
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildAssets: function (huid) {
        var context = this;

        context.$('.major').empty();

        var entries = getListEntry(huid);
        if (!entries)
            return;
		var cnt = 0;
        var assets = this.assets = entries.DataArea;
		this.$('.major').append('<a class="back-button" href="#" title="back" data-translate="back">< BACK</a>');
        if (assets && $.isArray(assets.ListOfEntry)) {
            $.each(assets.ListOfEntry, function (i, entry) {
                if (entry.tagName == 'Asset') {
					cnt = cnt + 1;
                    var isBookmarked = (entry.tagAttribute.ticketIDList && entry.tagAttribute.ticketIDList != '');
                    var $entry = $('<a href="#" class="asset text-med"></a>').attr('data-index', i).attr('title', entry.ListOfMetaData.Title).text(entry.ListOfMetaData.Title)
                    if (isBookmarked)
                        $entry.addClass('bookmarked');
                    $entry.appendTo(context.$('.major'));
                }
            });

            // pad some extra lines to fill the last page
            var padNum = this.pageSize - (assets.ListOfEntry.length % this.pageSize);
            if (padNum == 6) padNum = 0;
            for (var i = 0; i < padNum; i++) {
                $('<div class="padding"></div>').appendTo(context.$('.sub-text1'));
            }
        }

        this.$('.major').show();        
    },

    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemIndex = $obj.attr('data-index');
        var entries = null;

        if ($obj.hasClass('folder'))
            entries = this.folders;
        else if ($obj.hasClass('asset'))
            entries = this.assets;

        if (itemIndex && entries && entries.ListOfEntry) {
            itemData = entries.ListOfEntry[itemIndex];
        }
        return itemData;
    }
});    