// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-meal panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load meal.html and meal.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function Meal (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    if(this.backAtBottom)
    	this.idx = 0;
    else
    	this.idx = 1;
}

Meal.prototype.render = function(data) {
	this.data = data;
	
	if(!this.noAjax) { // Ajax loading
    	// load meal.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/meal.css"}).appendTo("head");
		
		// load meal.html
		var container = $('#' + this.wrapper);

		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("./templates/meal.html #meal", function(){            // callback to outside so they can do stuff (such as hiding div)            if(context.oncreate)                context.oncreate();                
			context._renderData();			applySettings('#meal');
		});
    }
    else {
    	this._renderData();
    }
}

Meal.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/meal.css"]').remove();
	
	// callback to outside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
Meal.prototype._navigate = function(key)	{

	var curr = $("#meal #overlay #agenda.active");
	
	if ( key=='MENU' || key =='HOME')	{
		this.destroy();		
		return false;
	}		if (key == 'ENTER')	{		this.destroy();				return true;	}
	if ( key=='POWR' ||	key=='END')	{		this.destroy();		return false;	}
	var menuprev = "UP", menunext = "DOWN";
	var submenuprev = "LEFT", submenunext = "RIGHT";
	if(this.horizontalMenu) {	// horizontal menu uses left/right to change focus and up/down to further control sub menu
		menuprev = "LEFT";                              
		menunext = "RIGHT";
		submenuprev = "UP";
		submenunext = "DOWN";
	}
	
	if(key==menuprev || key==menunext) {
		this._changeDataFocus(key);	
		return true;
	}
	else if(key==submenuprev || key==submenunext) {
		this._changeSubDataFocus(key);	
		return true;
	}
	
	return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
Meal.prototype._click = function(jqobj) {
	return;
};

// When an object is focused by using keyboard, or mouse hover
Meal.prototype._focus = function(jqobj) {
    if(jqobj) {
	   jqobj.addClass('active');
	   this.idx = jqobj.index();
	}
}

// When an object lose focus or mouse leave
Meal.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {
		$('div#meal div#overlay div#agenda.active').removeClass('active');
	}
	this.idx = -1;
}

Meal.prototype._renderData = function() {
    $('#' + this.wrapper + " #meal").addClass(this.classStr);
    var data = this.data;
    
	// show the div
	$('#' + this.wrapper + " #meal").show();
	
	var htmlStr = "";

    if(!data.response || !data.response.agendas || !data.response.agendas.agenda) {
        if(console) console.error('Incorrect agenda data!');
        return;
    }
    
    var agendaList = data.response.agendas.agenda;
    for(var i=0; i<agendaList.length; i++) {
        var agenda = agendaList[i];		var date = new Date(agenda.date + 'T12:00:00');		var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];		var dateStr = date.toLocaleDateString();
        htmlStr += '<div id="agenda"><div id="date">' + dateStr + '</div>';
        $('#meal #overlay #date').html(agenda.date);
        if(agenda.entry) {
            htmlStr += agenda.entry.content;
        }
        htmlStr += '</div>';
    }
    
    $('#meal div#overlay #content').html(htmlStr);
    
    var firstobj = $('#meal div#overlay #content div#agenda:first-child');
    this._focus(firstobj);
    
    // now we take over the key handler from navigation.js
    this._overwriteKeypressed();
};

Meal.prototype._renderSubData = function(jqobj) {
};

// Hijack the global keypressed() function
Meal.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
Meal.prototype._restoreKeypressed = function() {
	window.keypressed = this.original_keypressed;
};

Meal.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	
	if(window['msg'])	
		msg('(meal.js?) keypress: code = ' + keyCode + ' - ' + akey  );
	
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
Meal.prototype._changeDataFocus = function(key) {

	var curr = $('div#meal div#overlay div#agenda.active');
	var next = curr;

	if(curr.length <= 0) {
		this.idx = -1;
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#meal div#overlay div#agenda:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#meal div#overlay div#agenda:first-child');
			break;
	}
	
	this._blur(curr);
	this._focus(next);
};

// Focus control
Meal.prototype._changeSubDataFocus = function(key) {
}

